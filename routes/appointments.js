const express = require('express');
const userController = require('../controllers/appointment');
const checkAuth = require('../middleware/check-auth');

const router = express.Router();

router.post('', checkAuth, userController.createAppointment);
router.patch('/:id', checkAuth, userController.rescheduleAppointment);
router.patch('/confirm-appointment/:id', checkAuth, userController.confirmAppointment);
router.patch('/cancel-appointment/:id', checkAuth, userController.cancelAppointment);
router.get('', checkAuth, userController.getAppointments);
router.get('/medapps', checkAuth, userController.getMedAppointments);
router.get('/:id', userController.getOneAppointment);
router.delete('/:id', checkAuth, userController.deleteAppointment);

module.exports = router;