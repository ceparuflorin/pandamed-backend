const express = require('express');
const userController = require('../controllers/user');
const checkAuth = require('../middleware/check-auth');

const router = express.Router();

router.post('/signup', userController.createUser );
router.post('/login', userController.loginUser );
router.get('/medic-list/:id', checkAuth, userController.medicList );
router.patch('/update-profile/:id', checkAuth, userController.updateProfile );
router.patch('/update-email/:id', checkAuth, userController.updateEmail );
router.patch('/grand-access/:id', checkAuth, userController.grandAccess );
router.delete('/delete-user/:id', checkAuth, userController.deleteUser, );
router.get('/profile-info/:id', userController.getProfileInfo );

module.exports = router;