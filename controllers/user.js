const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/user');
const Appointment = require('../models/appointment');


exports.createUser = (req, res, next) => {
    bcrypt.hash(req.body.password, 10)
    .then(hash => {
        user = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            cnp: req.body.cnp,
            birthDate: req.body.birthDate,
            email: req.body.email,
            password: hash,
            role: 'patient'
        });
        user.save()
        .then(result => {
            res.status(201).json({message: 'User created successfuly!', result: result});
        })
        .catch(err => {
            res.status(500).json({message: 'Email address already taken. Please log in!', error: err});
        });
    });
}

exports.loginUser = (req, res, next) => {
    let fatchedUser;
    User.findOne({ email: req.body.email }).then(user => {
        if(!user) {
            return res.status(404).json({
                message: 'User is not registered!'
            });
        }
        fatchedUser = user;
        return bcrypt.compare(req.body.password, user.password);
    })
    .then(result => {
        if(!result) {
            return res.status(401).json({
                message: 'Wrong credentials!'
            });
        }
        const token = jwt.sign(
            {email: fatchedUser.email, userId: fatchedUser._id},
            process.env.JWT_KEY,
            { expiresIn: '1h' }
        )
        res.status(200).json({
            token: token,
            expiresIn: 3600,
            userId: fatchedUser._id,
            role: fatchedUser.role
        });
    })
    .catch(err => {
        return res.status(401).json({
            message: 'Failed to login into your account! Please try again!'
        });
    });
}

exports.updateProfile = (req, res, next) => {
    User.updateOne({ _id: req.params.id}, { $set: {firstName: req.body.firstName, lastName: req.body.lastName}} )
    .then( result => {
        console.log(result);
        if (result.n > 0) {
            res.status(200).json({message: "User's profile updated successfuly!"});
        } else {
            res.status(401).json({message: "Profile info not updated. Please try again!"});
        }
    })
    .catch(error => {
        res.status(500).json({
            message: "Profile couldn't be updated. Please try again later!"
        })
    });
}

exports.updateEmail = (req, res, next) => {
    User.updateOne({ _id: req.params.id}, { $set: {email: req.body.email}} )
    .then( result => {
        console.log(result);
        if (result.n > 0) {
            res.status(200).json({message: "Email address updated successfuly!"});
        } else {
            res.status(401).json({message: "Email address not updated. Please try again!"});
        }
    })
    .catch(error => {
        res.status(500).json({
            message: "Account couldn't be updated. Please try again later!"
        })
    });
}

exports.getProfileInfo = (req, res, next) => {
    User.find({_id: req.params.id}).then(document => {
        res.status(200).json({
            message: 'Got the user info from backend!',
            profileInfo: document
        });
    })
    .catch(error => {
        res.status(500).json({
            message: 'Something went wrong. No appointments found!'
        })
    });
}

exports.grandAccess = (req, res, next) => {
    User.updateOne({cnp: req.body.cnp}, { $set: {role: 'medic'}} )
    .then( result => {
        console.log(result);
        if (result.n > 0) {
            res.status(200).json({message: "Access granted successfuly!"});
        } else {
            res.status(401).json({message: "The medic is not is our database. Please register it first!"});
        }
    })
    .catch(error => {
        res.status(500).json({
            message: "Access couldn't be granted. Please try again later!"
        })
    });
}

exports.medicList = (req, res, next) => {
    User.find( {role: 'medic', _id : {$ne: req.params.id }} ).then(documents => {
        res.status(200).json({
            message: 'Got the medic list from backend!',
            medicList: documents
        });
    })
    .catch(error => {
        res.status(500).json({
            message: 'Something went wrong. No medic found!'
        });
    });
}

exports.deleteUser = (req, res, next) => {
    let userId = req.userData.userId;
    let message = '';
    User.deleteOne({_id: req.params.id}).then( result => {
        Appointment.deleteMany({patientId: userId}).then( result => {
            if (result.n > 0) {
                this.message += 'All Appointments deleted successfuly! - ';
            } else {
                this.message += 'You are not authorized to detele any appointments!';
            }
        })
        .catch(error => {
            this.message += 'Something went wrong when deleting the appointments. Please try again later!';
        });

        if (result.n > 0) {
            this.message += 'User deleted successfuly!';
            res.status(200).json({message: this.message});
        } else {
            this.message =+ 'You are not authorized to detele this user!';
            res.status(401).json({message: this.message});
        }
    })
    .catch(error => {
        this.message += 'Something went wrong when deleting the user. Please try again later!';
        res.status(500).json({message: this.message})
    })
}