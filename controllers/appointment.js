const Appointment = require('../models/appointment');

exports.createAppointment = (req, res, next) => {
    const appointment = new Appointment({
        hospital: req.body.hospital,
        department: req.body.department,
        medicId: req.body.medicId,
        appointmentDate: req.body.appointmentDate,
        additionalDetails: req.body.additionalDetails,
        patientId: req.userData.userId,
        appointmentStatus: 'Pending'
    });
    appointment.save().then(result => {
        res.status(201).json({
            message: "Appointment created successfuly!",
            appointmentId: result._id
        });
    })
    .catch(error =>{
        res.status(500).json({
            message: "Appointment couldn't be created. Please try again later!"
        })
    });
}

exports.rescheduleAppointment = (req, res, next) => {
    Appointment.updateOne({ _id: req.params.id, patientId: req.userData.userId }, { $set: {appointmentDate: req.body.date, appointmentStatus: 'Pending'}} )
    .then( result => {
        if (result.n > 0) {
            res.status(200).json({message: 'Appointment Rescheduled!'});
        } else {
            res.status(401).json({message: "No appointment rescheduled. Please try again later!"});
        }
    })
    .catch(error => {
        res.status(500).json({
            message: "Appointment couldn't be rescheduled. Please try again later!"
        })
    });
}

exports.confirmAppointment = (req, res, next) => {
    Appointment.updateOne({ _id: req.params.id }, { $set: {appointmentStatus: 'Confirmed'}} )
    .then( result => {
        if (result.n > 0) {
            res.status(200).json({message: 'Appointment Confirmed!'});
        } else {
            res.status(401).json({message: "Appointment not confirmed. Please try again later!"});
        }
    })
    .catch(error => {
        res.status(500).json({
            message: "Appointment couldn't be confirmed. Please try again later!"
        })
    });
}

exports.cancelAppointment = (req, res, next) => {
    Appointment.updateOne({ _id: req.params.id }, { $set: {appointmentStatus: 'Cancelled'}} )
    .then( result => {
        if (result.n > 0) {
            res.status(200).json({message: 'Appointment Cancelled!'});
        } else {
            res.status(401).json({message: "Appointment not cancelled. Please try again later!"});
        }
    })
    .catch(error => {
        res.status(500).json({
            message: "Appointment couldn't be cancelled. Please try again later!"
        })
    });
}

exports.getAppointments = (req, res, next) => {
    Appointment.find( {patientId: req.userData.userId} ).sort({ appointmentDate : 'asc'}).then(documents => {
        res.status(200).json({
            message: 'Got the appointments from backend!',
            appointments: documents
        });
    })
    .catch(error => {
        res.status(500).json({
            message: 'Something went wrong. No appointments found!'
        });
    });
}

exports.getMedAppointments = (req, res, next) => {
    Appointment.find( {medicId: req.userData.userId} ).then(documents => {
        res.status(200).json({
            message: 'Got the med appointments from backend!',
            appointments: documents
        });
    })
    .catch(error => {
        res.status(500).json({
            message: 'Something went wrong. No med appointments found!'
        });
    });
}

exports.getOneAppointment = (req, res, next) => {
    Appointment.find({_id: req.params.id}).then(document => {
        res.status(200).json({
            message: 'Got one appointment from backend!',
            appointment: document
        });
    })
    .catch(error => {
        res.status(500).json({
            message: 'Something went wrong. No appointments found!'
        })
    });
}

exports.deleteAppointment = (req, res, next) => {
    Appointment.deleteOne({_id: req.params.id, patientId: req.userData.userId}).then( result => {
        if (result.n > 0) {
            res.status(200).json({message: 'Appointment deleted successfuly!'});
        } else {
            res.status(401).json({message: 'You are not authorized to detele this appointment!'});
        }
    })
    .catch(error => {
        res.status(500).json({
            message: 'Something went wrong when deleting the appointment. Please try again later!'
        })
    });
}