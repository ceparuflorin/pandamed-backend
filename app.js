const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cors = require('cors');

const appointmentsRoutes = require('./routes/appointments');
const userRoutes = require('./routes/user');

const app = express();

mongoose.connect('mongodb+srv://med-app:' + process.env.MONGO_ATLAS_PW + '@medappcluster-3s8qn.mongodb.net/med-app?retryWrites=true&w=majority', { useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex:true })
    .then(() => {
        console.log('Connected to the Database!')
    })
    .catch(() => {
        console.log('Connect to Database failed!')
    });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cors());

app.use((req, res, next) => {
    res.setHeader("Access-Control-Allow-Origin", "*");
    res.setHeader("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
    res.setHeader("Access-Control-Allow-Methods", "GET, POST, PATCH, DELETE, OPTIONS");
    next();
});

app.use("/api/appointments/", appointmentsRoutes);
app.use("/api/user/", userRoutes);

module.exports = app;