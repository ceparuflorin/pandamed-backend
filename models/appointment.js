const mongoose = require("mongoose");

const appointmentSchema = mongoose.Schema({
  hospital: { type: String, require: true },
  department: { type: String, require: true },
  medicId: { type: mongoose.Schema.Types.ObjectId, ref: "User", require: true },
  appointmentDate: { type: Date, require: true },
  additionalDetails: { type: String, default: "No Details" },
  patientId: { type: mongoose.Schema.Types.ObjectId, ref: "User", require: true},
  appointmentStatus: { type: String }
});

module.exports = mongoose.model("Appointment", appointmentSchema);
