const mongoose = require("mongoose");

const uniqueValidator = require("mongoose-unique-validator");

const userSchema = mongoose.Schema({
  firstName: { type: String, require: true },
  lastName: { type: String, require: true },
  cnp: { type:Number, require:true, unique: true },
  birthDate: { type:Date, require:true },
  email: { type: String, require: true, unique: true },
  password: { type: String, required: true },
  role: { type: String, required: true },
});

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model("User", userSchema);
